class CreateDuck1s < ActiveRecord::Migration[6.1]
  def change
    create_table :duck1s do |t|
      t.string :title
      t.text :body

      t.timestamps
    end
  end
end
