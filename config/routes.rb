Rails.application.routes.draw do
  root "ducks#index"

  get "/ducks", to: "ducks#index"
  get "/ducks/:id", to: "ducks#show"
end